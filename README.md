# epubify

Epubify is a clojure application that can convert a URL into an epub file for offline reading.
This application is meant for personal use only and should never be used to distribute works
that you do not own.

Dedicated to all the fiction I've been able to read for free online, both the ones I've enjoyed
and the ones I didn't. Your dedication has enabled many hours of escapism and amusement. Thanks!

## Installation

TODO

## Usage

TODO

## Options

TODO

## Examples

...
