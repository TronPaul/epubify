(defproject epubify "0.1.0-SNAPSHOT"
  :description "Website to epub converter"
  :url "https://gitlab.com/TronPaul/epubify"
  :license {:name "The MIT License"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [hickory "0.7.1" :exclusions [[org.clojure/clojurescript]
                                               [viebel/codox-klipse-theme]]]
                 [clj-http "3.10.0"]
                 [org.clojure/data.xml "0.2.0-alpha6"]
                 [digest "1.4.9"]
                 [com.chimbori.crux/crux "2.1.0"]]
  :main ^:skip-aot epubify.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :plugins [[lein-licenses "0.2.2"]])
