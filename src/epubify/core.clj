(ns epubify.core
  (:require [epubify.royalroad :as rr]
            [epubify.readability :as r]
            [epubify.epub :as epub]
            [epubify.zip :refer [zip-stream]])
  (:gen-class))

(defn url-to-epub
  [url filename]
  (if-let [book-definition (cond
                             (rr/valid-url? url) (rr/create-book-definition url)
                             (r/valid-url? url) (r/create-book-definition url)
                             :else nil)]
    (with-open [output (zip-stream filename)]
      (epub/epub output book-definition))
    nil))

(defn -main
  "I don't do a whole lot ... yet."
  [& [url filename]]
  (url-to-epub url filename))
