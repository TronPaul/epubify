(ns epubify.epub
  (:use [epubify.zip])
  (:require [clojure.data.xml :as dxml]
            [hickory.render :refer [hickory-to-html]]
            [clojure.java.io :as io]
            digest
            [epubify.utils :as utils]
            [clojure.spec.alpha :as s])
  (:import (java.io File)
           (sun.net.www MimeTable)))

(def mimetype "application/epub+zip")

(dxml/alias-uri 'dc "http://purl.org/dc/elements/1.1/")
(dxml/alias-uri 'dcterms "http://purl.org/dc/terms/")
(dxml/alias-uri 'xsi "http://www.w3.org/2001/XMLSchema-instance")
(dxml/alias-uri 'xml "http://www.w3.org/XML/1998/namespace")

(def container
  (dxml/element
    (dxml/qname "urn:oasis:names:tc:opendocument:xmlns:container" "container")
    {:xmlns "urn:oasis:names:tc:opendocument:xmlns:container" :version "1.0"}
    (dxml/element
      (dxml/qname "urn:oasis:names:tc:opendocument:xmlns:container" "rootfiles")
      {}
      (dxml/element
        (dxml/qname "urn:oasis:names:tc:opendocument:xmlns:container" "rootfile")
        {:media-type "application/oebps-package+xml"}))))

(def opf-qname
  (partial dxml/qname "http://www.idpf.org/2007/opf"))

(defn- chapter-filename
  [i]
  (format "chapter-%d.html" i))

(defn- chapter-name
  [i]
  (format "chapter-%d" i))

(defn content-opf
  [book-defn]
  (dxml/element
    (opf-qname "package")
    {:xmlns "http://www.idpf.org/2007/opf"
     :xmlns/dc "http://purl.org/dc/elements/1.1/"
     :xmlns/dcterms "http://purl.org/dc/terms/"
     :xmlns/xsi "http://www.w3.org/2001/XMLSchema-instance"
     :version "2.0"
     :unique-identifier "id"}
    (dxml/element
      (opf-qname "metadata")
      {}
      (dxml/element ::dc/rights {} "Copywritten")
      (dxml/element ::dc/date {"opf:event" "publication"} "DATE")
      (dxml/element ::dc/date {"opf:event" "conversion"} "DATE")
      (dxml/element ::dc/source {} "https://example.com")
      (dxml/element (opf-qname "meta") {:name "cover" :content "item1"}))
    (dxml/element
      (opf-qname "manifest")
      {}
      (dxml/element (opf-qname "item") {:media-type "application/xhtml+xml" :href "wrap.html" :id "coverpage-wrapper"})
      (dxml/element (opf-qname "item") {:media-type "x-dtbncx+xml" :href "tox.ncx" :id "ncx"})
      (doall (map #(dxml/element (opf-qname "item") {:media-type "application/xhtml+xml" :href (chapter-filename %1) :id (chapter-name %1)}) (range (count (::chapters book-defn))))))
    (dxml/element
      (opf-qname "spine")
      {:toc "ncx"}
      (dxml/element (opf-qname "itemref") {:linear "no" :idref "coverpage-wrapper"})
      (doall (map #(dxml/element (opf-qname "itemref") {:linear "yes" :idref (chapter-name %1)}) (range (count (::chapters book-defn))))))
    (dxml/element
      (opf-qname "guide")
      {}
      (dxml/element (opf-qname "reference") {:title "Cover" :href "wrap.html" :type "cover"}))))

(def ncx-qname
  (partial dxml/qname "http://www.daisy.org/z3986/2005/ncx/"))

(defn toc
  [book-defn]
  (dxml/element
    (ncx-qname "ncx")
    {:xmlns "http://www.daisy.org/z3986/2005/ncx/"
     :version "2005-1"
     ::xml/lang "en"}
    (dxml/element
      (ncx-qname "head")
      {}
      (dxml/element
        (ncx-qname "meta")
        {:name "dtb:uid" :content "URL"})
      (dxml/element
        (ncx-qname "meta")
        {:name "dtb:depth" :content "2"})
      (dxml/element
        (ncx-qname "meta")
        {:name "dtb:generator" :content "epubify"})
      (dxml/element
        (ncx-qname "meta")
        {:name "dtb:totalPageCount" :content "0"})
      (dxml/element
        (ncx-qname "meta")
        {:name "dtb:maxPageNumber" :content "0"}))
    (dxml/element
      (ncx-qname "docTitle")
      {}
      (dxml/element
        (ncx-qname "text")
        {}
        (:title book-defn)))
    (dxml/element
      (ncx-qname "navMap")
      {}
      (doall (map-indexed (fn [i chapter-def]
                            (dxml/element
                              (ncx-qname "navPoint")
                              {:playOrder (str i)
                               :id (format "np-%d" i)}
                              (dxml/element
                                (ncx-qname "navLabel")
                                {}
                                (dxml/element
                                  (ncx-qname "text")
                                  {}
                                  (::title chapter-def)))
                              (dxml/element
                                (ncx-qname "content")
                                {:src (chapter-filename i)}))) (::chapters book-defn))))))

(defn cover
  [title]
  {:type :document :content [{:type :element :attrs nil :tag :html :content [{:type :element :attrs nil :tag :head :content [{:type :element :attrs nil :tag :title :content ["Cover"]}
                                                                                                                             {:type :element :attrs nil :tag :body :content [{:type :element :attrs nil :tag :h1 :content [title]}]}]}]}]})

(defn replace-src
  [media element]
  (update-in element [:attrs :src] #(get media %1)))

; TODO: tail recursion here
(defn update-content
  [pred f content]
  (map (fn [element]
         (if (map? element)
           (update-in (if (pred element)
                        (f element)
                        element) [:content] (partial update-content pred f))
           element)) content))

(defn chapter
  [document media]
  (update-in document [:content] (partial update-content utils/is-media? (partial replace-src media))))

(def mime-table
  (MimeTable/loadTable))

(defn collect-and-write-media
  [stream media]
  (loop [i 0
         written-media-src {}
         written-media-md5 {}
         cur (first media)
         media-to-write (rest media)]
    (cond
      (nil? cur) written-media-src
      (contains? written-media-src (::src cur)) (recur i written-media-src written-media-md5 (first media-to-write) (rest media-to-write))
      :else (let [extension (first (.getExtensions (.find mime-table (::mime-type cur)))) ;this extension is crap (jpeg -> jfif) find something better
                  tmp-file (File/createTempFile "media" extension)]
              (with-open [tmp-output (io/output-stream tmp-file)
                          src-input (io/input-stream (::src cur))]
                (io/copy src-input tmp-output))
              (let [md5 (digest/md5 tmp-file)]
                (if (contains? written-media-md5 md5)
                  (recur i (assoc written-media-src (::src cur) (get written-media-md5 md5)) written-media-md5 (first media-to-write) (rest media-to-write))
                  (let [relative-filename (format "%d%s" i extension)]
                    (create-zip-entry stream (str "OEBPS/" relative-filename))
                    (with-open [tmp-input (io/input-stream tmp-file)]
                      (io/copy tmp-input stream))
                    (.closeEntry stream)
                    (recur (inc i) (assoc written-media-src (::src cur) relative-filename) (assoc written-media-md5 md5 relative-filename) (first media-to-write) (rest media-to-write)))))))))

(defn epub
  [stream book]
  (create-zip-entry stream "mimetype")
  (write-to-zip stream mimetype)
  (.closeEntry stream)
  (create-zip-entry stream "META-INF/container.xml")
  (write-to-zip stream (dxml/emit-str container))
  (.closeEntry stream)
  (create-zip-entry stream "OEBPS/toc.ncx")
  (write-to-zip stream (dxml/emit-str (toc book) :doctype "ncx PUBLIC '-//NISO//DTD ncx 2005-1//EN' 'http://www.daisy.org/z3986/2005/ncx-2005-1.dtd'"))
  (.closeEntry stream)
  (let [src-to-file (collect-and-write-media stream (flatten (map ::media (::chapters book))))]
    (create-zip-entry stream "OEBPS/content.opf")
    (write-to-zip stream (dxml/emit-str (content-opf book)))
    (.closeEntry stream)
    (create-zip-entry stream "OEBPS/wrap.html")
    (write-to-zip stream (hickory-to-html (cover (::title book))))
    (.closeEntry stream)
    (doall (map-indexed (fn [i chapter-def]
                          (create-zip-entry stream (str "OEBPS/" (chapter-filename i)))
                          (write-to-zip stream (hickory-to-html (chapter (::document chapter-def) src-to-file)))
                          (.closeEntry stream))
                        (::chapters book)))))

(s/def ::src string?)
(s/def ::mime-type string?)
(s/def ::media (s/keys :req [::src ::mime-type]))

(s/def ::type keyword?)
(s/def ::attrs (s/map-of keyword? string?))
(s/def ::content (s/nilable ::hickory))
(s/def ::hickory (s/keys :req-un [::type ::attrs ::content]))

(s/def ::title string?)
(s/def ::document ::hickory)
(s/def ::medias (s/coll-of ::media))
(s/def ::chapter (s/keys :req [::title ::document ::medias]))

(s/def ::chapters (s/coll-of ::chapter))
(s/def ::book (s/keys :req [::title ::chapters]))

(s/fdef epub
        :args (s/cat :stream (s/and (s/and not nil?) (partial instance? java.util.zip.ZipOutputStream)) :book ::book)
        :ret nil?)