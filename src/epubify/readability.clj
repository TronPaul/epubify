(ns epubify.readability
  (:require [clj-http.client :as client]
            [hickory.core :as hickory])
  (:import (com.chimbori.crux.articles ArticleExtractor)
           (com.chimbori.crux.urls CruxURL)))

(defn valid-url? [url]
  (-> (CruxURL/parse url)
      (.isLikelyArticle)))

(defn article-extractor [body url]
  (ArticleExtractor/with url body))

(defn get-article [url]
  (-> (client/get url)
      (:body)
      (article-extractor url)
      (.extractMetadata)
      (.extractContent)
      (.article)))

;TODO handle flat (single chapter) ebooks
(defn create-book-definition
  [url]
  (let [article (get-article url)
        title (.title article)]
    {:epubify.epub/title title
     :epubify.epub/chapters [{:epubify.epub/title title
                              :epubify.epub/document (hickory/as-hickory (.document article))}]}))
