(ns epubify.royalroad
  (:require [hickory.select :as s]
            [epubify.utils :as utils]))

(def url-regex #"https://www\.royalroad\.com/fiction/[0-9]+/[a-z0-9-]+")

(defn fiction-title [htree]
  (->
    (s/select (s/descendant (s/and (s/attr :property (partial = "name")) (s/tag :h1))) htree)
    first
    :content
    first))

(defn fiction-chapters [htree]
  (->>
    (s/select (s/descendant (s/id "chapters") (s/and (s/tag :a) (s/attr :href))) htree)
    (map #(str "https://www.royalroad.com" (get-in % [:attrs :href])))))

; TODO classify styling
; * author notes
; * desired styling for ebook
(defn remove-style [m]
  (if (map? m)
    (-> m
        (update-in [:attrs] dissoc :style)
        (update-in [:content] (partial map remove-style)))
    m))

(defn chapter-title [htree]
  (->
    (s/select (s/descendant (s/class "fic-header") (s/tag :h1)) htree)
    first
    :content
    first))

(def chapter-selector
  (s/and (s/class "chapter-content") (s/class "chapter-inner")))

(defn chapter-content [htree]
  (->> htree
       (s/select chapter-selector)
       first
       remove-style
       :content))

(defn chapter-media [htree]
  (->> htree
      (s/select (s/descendant chapter-selector (s/tag :img)))
      (map utils/hickory-to-media)))

(defn chapter [htree]
  (let [title (chapter-title htree)]
    {:epubify.epub/title title :epubify.epub/document (utils/body-to-document title (chapter-content htree)) :epubify.epub/media (chapter-media htree)}))

(defn valid-url?
  [url]
  (re-matches url-regex url))

(defn create-book-definition
  [fiction-url]
  (let [fiction-htree (utils/htree (re-find url-regex fiction-url))]
    {:epubify.epub/title (fiction-title fiction-htree)
     :epubify.epub/chapters (map #(chapter (utils/htree %)) (fiction-chapters fiction-htree))}))
