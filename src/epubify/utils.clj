(ns epubify.utils
  (:require [clj-http.client :as client]
            [hickory.core :as hickory]
            [clojure.java.io :refer [input-stream]])
  (:import [java.net URLConnection]))

(defn htree
  [url]
  (-> (client/get url) :body hickory/parse hickory/as-hickory))

(defn mime-via-stream
  [url]
  (with-open [s (input-stream url)]
    (URLConnection/guessContentTypeFromStream s)))

(defn img-to-media
  [img]
  (let [src (get-in img [:attrs :src])]
    {:epubify.epub/src src
     :epubify.epub/mime-type (first (filter #(not (nil? %)) (map (fn [f]
                                                      (f src)) [#(URLConnection/guessContentTypeFromName %1) mime-via-stream])))}))

; TODO: make the cond -> pred abstract and usable for is-media?
(defn hickory-to-media
  [element]
  (cond
    (and (= (:tag element) :img) (get-in element [:attrs :src])) (img-to-media element)
    :else nil))

(defn is-media?
  [element]
  (and (= (:tag element) :img) (get-in element [:attrs :src])))

(defn body-to-document
  [title body-content]
  {:type :document :content [{:type :element :attrs nil :tag :html :content [{:type :element :attrs nil :tag :head :content [{:type :element :attrs nil :tag :title :content [title]}]}
                                                                             {:type :element :attrs nil :tag :body :content (cons {:type :element :attrs nil :tag :h1 :content [title]} body-content)}]}]})
