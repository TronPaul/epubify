(ns epubify.zip
  (:import [java.io FileOutputStream FileOutputStream BufferedOutputStream]
            [java.util.zip ZipOutputStream ZipEntry]))

(defn zip-stream
  "Opens a ZipOutputStream over the given file (as a string)"
  [file]
  (-> (FileOutputStream. file)
      BufferedOutputStream.
      ZipOutputStream.))

(defn create-zip-entry
  "Create a zip entry with the given name. That will be the name of the file inside the zipped file."
  [stream entry-name]
  (.putNextEntry stream (ZipEntry. entry-name)))

(defn write-to-zip
  "Writes a string to a zip stream as created by zip-stream"
  [stream str]
  (.write stream (.getBytes str)))